document.body.onload = function (e) {
    //create fullscreen dom element
    var fullDiv = document.createElement("div");
    fullDiv.id = "modal-kilogram";
    fullDiv.style.position = 'absolute';
    fullDiv.style.left = 0;
    fullDiv.style.top = 0;
    fullDiv.style.opacity = 1;
    fullDiv.style.backgroundColor = 'white';
    console.log("document.documentElement.clientWidth " + document.documentElement.clientWidth);
    document.body.appendChild(fullDiv);
}

// listen for clicks
document.addEventListener("click", function (event) {

    //close modal if open
    var modal = document.getElementById("modal-kilogram");
    if (modal.style.opacity > 0.9) {
        //set modal to 0x0 bcs of click listening - that would ruin everything!
        modal.style.opacity = 0;
        modal.style.width = '0px';
        modal.style.height = '0px';
        //remove previous img items
        while (modal.hasChildNodes()) {
            modal.removeChild(modal.lastChild);
        }
    }

    // do we have image in the sibling?
    if (!event.target.classList.contains("_9AhH0")) {
        return
    }

    // if instagram modal is already presented, return
    if (document.getElementsByClassName("JyscU").length > 0) {
        return
    }

    // show modal if possible
    var img = event.target.parentElement.getElementsByTagName("img")[0];
    modal.style.top = window.pageYOffset + 'px';
    modal.style.opacity = 1;
    modal.style.width = document.documentElement.clientWidth + 'px';
    modal.style.height = document.documentElement.clientHeight + 'px';
    modal.style.overflow = 'hidden';

    var imageElement = document.createElement("img");
    imageElement.setAttribute("src", img.src);
    // center it into middle of screen
    imageElement.style.margin = 'auto';
    imageElement.style.maxHeight = document.documentElement.clientHeight + 'px';
    modal.appendChild(imageElement);

});
